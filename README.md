# What is this ?

This repository is a small test to create a simple little game of pong to run it on my raspberry
 pi 1B. Maybe with docker?

## How to

This is meant to work with Python 3. Before I can get any docker working, use venv to make it work

    virtualenv venv
    . ./venv/bin/activate
    pip install -r requirements.txt
    python pong/main.py

For now, it might only work on the raspberry pi as it expects the gpizero module to be installed
 and the GPIO pins to be available. 


## Docker command that works

### To build:
    docker build -t pong .
    
### To run the currently packaged version
    docker run --net=host --env="DISPLAY" --volume="$HOME/.Xauthority:/root/.Xauthority:rw" 
    bpaccaud/pong:latest

