FROM python:3

ADD pong /
ADD requirements.txt /

RUN pip install -r requirements.txt

CMD ["python", "./main.py"]

