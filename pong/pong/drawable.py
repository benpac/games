import abc


class Drawable(abc.ABC):
    @abc.abstractmethod
    def draw(self, surface, color):
        pass
