"""
Main class containing the game loop and such
"""
import time

import pygame

from . import *

try:
    from .input.gpio import get_interface as io
except ImportError:
    from .input.keyboard import keyboard_interface as io


class Pong:
    SCORE_HEIGHT = 75

    def __init__(self, height, width, fg_color=pygame.Color("white"),
                 bg_color=pygame.Color("black")):
        self._running = True
        self._display_surf = None
        self.size = self.width, self.height = height, width
        self.fg_color = fg_color
        self.bg_color = bg_color
        self.ball = None
        self.left_pallet = None
        self.right_pallet = None
        self.game_area = None
        self.scores = None
        self.drawables = []
        self.interface = io.get_interface()

    def on_init(self):
        pygame.init()
        self._display_surf = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        self._running = True

        self.game_area = GameArea(
            (0, self.SCORE_HEIGHT), (self.width, self.height - self.SCORE_HEIGHT,))
        self.drawables.append(self.game_area)
        self.scores = Scores(top_left=(0, 0), size=(self.width, self.SCORE_HEIGHT))
        self.drawables.append(self.scores)

        return True

    def on_restart(self):
        self.scores.reset()
        self.game_area.reinit_ball()

    def on_event(self, event):
        if event.type == pygame.QUIT:
            self._running = False

    def on_loop(self):
        if self.interface.is_restart():
            self.on_restart()
        left_move, right_move = self.interface.get_moves()
        result = self.game_area.update(left_move, right_move)
        self.scores.update(result)
        if sum(result) > 0:
            self.interface.light_up(*result)
            time.sleep(2)
            self.interface.lights_off()

    def on_render(self):
        self._display_surf.fill(self.bg_color)
        for drawable in self.drawables:
            drawable.draw(self._display_surf, self.fg_color)
        pygame.display.flip()

    def on_cleanup(self):
        pygame.quit()

    def on_execute(self):
        if not self.on_init():
            self._running = False

        while self._running:
            for event in pygame.event.get():
                self.on_event(event)
            self.on_loop()
            self.on_render()
        self.on_cleanup()
