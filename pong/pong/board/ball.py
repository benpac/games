import math
from typing import List

import pygame

from ..drawable import Drawable


class Ball(Drawable):
    def __init__(self, radius):
        self._x = None
        self._y = None
        self._vx = None
        self._vy = None
        self._radius = radius
        self._half_rect_size = math.sqrt(2) * radius

    @property
    def position(self):
        return pygame.Vector2(self._x, self._y)

    @position.setter
    def position(self, value):
        assert len(value) == 2
        self._x = value[0]
        self._y = value[1]

    @property
    def velocity(self):
        return pygame.Vector2(self._vx, self._vy)

    @velocity.setter
    def velocity(self, value):
        assert len(value) == 2
        self._vx = value[0]
        self._vy = value[1]

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        self._y = value

    @property
    def vx(self):
        return self._vx

    @property
    def vy(self):
        return self._vy

    @property
    def top(self):
        return self._y - self._radius

    @property
    def bottom(self):
        return self._y + self._radius

    @property
    def rect(self):
        return pygame.Rect(self._x - self._half_rect_size, self._y - self._half_rect_size,
                           *[2*self._half_rect_size]*2)

    def draw(self, surface, color):
        pygame.draw.circle(surface, color, self.position, self._radius)

    def intersects(self, line):
        return len(self.rect.clipline(*line)) > 0

    def move(self, horizontal_lines, vertical_lines, goals):
        # Not a great collision detection for now. But I can not be bothered right now.
        self.position += self.velocity
        for line in horizontal_lines:
            if self.intersects(line):
                self._vy *= -1
        for line in vertical_lines:
            if self.intersects(line):
                self._vx *= -1
        results = [0] * len(goals)
        for index, goal in enumerate(goals):
            if self.intersects(goal):
                results[index] = 1

        return results
