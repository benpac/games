import pygame

from .score import Score
from ..drawable import Drawable


class Scores(Drawable, pygame.Rect):
    def __init__(self, top_left, size):
        super().__init__(top_left, size)
        score_args = (0, (self.center[0], self.top - 5), (self.width // 2 - 10, self.height - 10))
        self._left_score = Score(*score_args, True)
        self._right_score = Score(*score_args, False)

    def reset(self):
        self._left_score.value = 0
        self._right_score.value = 0

    def update(self, result):
        left, right = result
        self._left_score.value += left
        self._right_score.value += right

    def draw(self, screen, color):
        self._left_score.draw(screen, color)
        pygame.draw.line(screen, color, (self.centerx, self.top), (self.centerx, self.bottom))
        self._right_score.draw(screen, color)
