import math

import pygame

from .ball import Ball
from .pallet import Pallet
from ..drawable import Drawable
from ..physics import random_unit_vector


class GameArea(Drawable, pygame.Rect):
    PALLET_OFFSET = 20
    BALL_RADIUS = 5
    PALLET_HEIGHT = 40
    PALLET_WIDTH = 2 * BALL_RADIUS
    SPEED = 3.0

    def __init__(self, top_left, size):
        super().__init__(top_left, size)
        self._border_thickness = 10
        self._ball = Ball(self.BALL_RADIUS)
        self.reinit_ball()
        self._left_pallet = Pallet(
            self.left + self.PALLET_OFFSET, self.PALLET_HEIGHT, self.PALLET_WIDTH, is_left=True)
        self._right_pallet = Pallet(
            self.right - self.PALLET_OFFSET, self.PALLET_HEIGHT, self.PALLET_WIDTH, is_left=False)
        self._left_pallet.y = self.center[1]
        self._right_pallet.y = self.center[1]

        self._children = []
        self._children.append(self._ball)
        self._children.append(self._left_pallet)
        self._children.append(self._right_pallet)

    def _hline(self, height):
        return self.left, height, self.right, height

    def _vline(self, x_pos):
        return x_pos, self.top, x_pos, self.bottom

    @property
    def top_line(self):
        return self._hline(self.top + self._border_thickness)

    @property
    def bottom_line(self):
        return self._hline(self.bottom - self._border_thickness)

    @property
    def left_goal(self):
        return self._vline(self.left)

    @property
    def right_goal(self):
        return self._vline(self.right)

    def reinit_ball(self):
        self._ball.position = self.center
        third_pi = math.pi / 3
        self._ball.velocity = random_unit_vector(
            ((-third_pi, third_pi), (2 * third_pi, 4 * third_pi))
        ) * GameArea.SPEED

    def draw(self, screen, color):
        pygame.draw.rect(screen, color, pygame.Rect(
            self.topleft, (self.width, self._border_thickness)
        ))
        pygame.draw.rect(screen, color, pygame.Rect(
            (self.left, self.bottom - self._border_thickness),
            (self.width, self._border_thickness)
        ))
        for child in self._children:
            child.draw(screen, color)
        pygame.draw.line(screen, color, (self.centerx, self.top), (self.centerx, self.bottom))

    def update(self, lp_movement, rp_movement):
        ball = self._ball
        if lp_movement is not None:
            self._left_pallet.move(lp_movement, self.top, self.bottom)
        if rp_movement is not None:
            self._right_pallet.move(rp_movement, self.top, self.bottom)

        result = ball.move(
            [self.top_line, self.bottom_line],
            [self._left_pallet.face, self._right_pallet.face],
            [self.right_goal, self.left_goal]
        )

        if sum(result) > 0:
            self.reinit_ball()

        return result

