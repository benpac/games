from .seven_segment_display import *
from ..drawable import *


class Score(Drawable):
    SPACE = 10

    def __init__(self, value, corner, max_size, right_aligned):
        assert isinstance(value, int)
        self._value = None
        self._digits = None
        self._displays = []
        self._individual_width = None
        self._corner = corner
        self._max_size = max_size

        self.value = value

    def __new__(cls, value, corner, max_size, right_aligned):
        if right_aligned:
            return super().__new__(ScoreRightAligned)
        else:
            return super().__new__(ScoreLeftAligned)

    @staticmethod
    def _get_digits(value):
        return [int(s) for s in str(value)]

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, v):
        self._value = v
        self._digits = self._get_digits(v)
        self._individual_width = min(
            (self._max_size[1] // 7) * 4, self._max_size[0] // len(self._digits))
        size = (self._individual_width, self._max_size[1])
        corners = [(corner_x, self._corner[1]) for corner_x in self._get_corners_x()]
        self._displays = [SevenSegmentDisplay(digit, corner, size) for corner, digit in zip(
            corners, self._digits)]

    @abc.abstractmethod
    def _get_corners_x(self):
        """Returns the corners in the order of the digits"""
        pass

    def draw(self, surface, color):
        for display in self._displays:
            display.draw(surface, color)


class ScoreRightAligned(Score):

    def __init__(self, value, corner, max_size, right_aligned):
        corner = (corner[0] - Score.SPACE, corner[1])
        super().__init__(value, corner, max_size, right_aligned)

    def _get_corners_x(self):
        base_x = self._corner[0]
        return [(base_x - self._individual_width) - i * (self._individual_width + Score.SPACE)
                for i in range(len(self._digits) - 1, -1, -1)]


class ScoreLeftAligned(Score):
    def __init__(self, value, corner, max_size, right_aligned):
        corner = (corner[0] + Score.SPACE, corner[1])
        super().__init__(value, corner, max_size, right_aligned)

    def _get_corners_x(self):
        return [self._corner[0] + i * (self._individual_width + Score.SPACE) for i in range(len(
            self._digits))]
