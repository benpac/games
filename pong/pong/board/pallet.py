import pygame

from ..drawable import Drawable


class Pallet(Drawable):
    def __init__(self, x, height, width, is_left):
        self._y = None
        self._x = x
        self._height = height
        self._width = width
        self._is_left = is_left

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        self._y = value

    @property
    def rect(self):
        return pygame.Rect(
            self._x - self._width//2, self.top,
            self._width, self._height
        )

    @property
    def top(self):
        return self._y - self._height//2

    @property
    def bottom(self):
        return self._y + self._height//2

    @property
    def face(self):
        x_face = self._x + (self._width//2 * (2*self._is_left - 1))
        return x_face, self.top, x_face, self.bottom

    def move(self, amount: int, max_h, min_h):
        self._y += amount
        if self.top < max_h:
            self._y = max_h + self._height // 2
        if self.bottom > min_h:
            self._y = min_h - self._height // 2

    def draw(self, screen, color):
        pygame.draw.rect(screen, color, self.rect)


