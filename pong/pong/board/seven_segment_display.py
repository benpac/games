import pygame

from ..drawable import Drawable


class SevenSegmentDisplay(Drawable, pygame.Rect):
    """Indexes of the segments:
        0
       ---
    1 |   | 2
       -3-
    4 |   | 5
       ---
        6
    """

    NUMBER_TO_INDEXES = {
        0: [0, 1, 2, 4, 5, 6],
        1: [2, 5],
        2: [0, 2, 3, 4, 6],
        3: [0, 2, 3, 5, 6],
        4: [1, 2, 3, 5],
        5: [0, 1, 3, 5, 6],
        6: [0, 1, 3, 4, 5, 6],
        7: [0, 2, 5],
        8: [0, 1, 2, 3, 4, 5, 6],
        9: [0, 1, 2, 3, 5, 6],
    }

    def __init__(self, value, top_left, size):
        super().__init__(top_left, size)
        self._value = value
        thickness = min(self.width // 4, self.height // 7)
        h_size = self.width, thickness
        v_size = thickness, (self.height // 2 + thickness // 2)

        self.segments = [
            pygame.Rect(self.topleft, h_size),
            pygame.Rect(self.topleft, v_size),
            pygame.Rect((self.right - thickness, self.top), v_size),
            pygame.Rect((self.left, self.bottom - v_size[1]), h_size),
            pygame.Rect((self.left, self.bottom - v_size[1]), v_size),
            pygame.Rect((self.right - thickness,  self.bottom - v_size[1]), v_size),
            pygame.Rect((self.left, self.bottom - thickness), h_size),
        ]

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, v):
        assert isinstance(v, int)
        assert 0 <= v <= 9
        self._value = v

    def draw(self, screen, color):
        indices = self.NUMBER_TO_INDEXES[self._value]
        for index in indices:
            pygame.draw.rect(screen, color, self.segments[index])
