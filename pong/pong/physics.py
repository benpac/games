import random
import math

import pygame


def random_unit_vector(angle_ranges=None):
    """Works best for uniform distribution if ranges are of equal length"""
    if angle_ranges is None:
        angle_range = (0, 2*math.pi)
    else:
        range_index = random.randint(0, len(angle_ranges)-1)
        angle_range = angle_ranges[range_index]
    angle = random.uniform(angle_range[0], angle_range[1])
    return pygame.Vector2(math.cos(angle), math.sin(angle))


