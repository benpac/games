import gpiozero as gz

from pong.pong.input.input_interface import InputInterface, PlayerAction
from .player_io import PlayerIO


class GPIOInterface(InputInterface):
    def __init__(self, left_player: PlayerIO, right_player: PlayerIO, restart_button: gz.Button):
        self.left = left_player
        self.right = right_player
        self.restart = restart_button

    def get_actions(self):
        actions = PlayerAction(0)
        if self.left.up.is_pressed:
            actions |= PlayerAction.LEFT_UP
        if self.left.down.is_pressed:
            actions |= PlayerAction.LEFT_DOWN
        if self.right.up.is_pressed:
            actions |= PlayerAction.RIGHT_UP
        if self.right.down.is_pressed:
            actions |= PlayerAction.RIGHT_DOWN

        return actions

    def light_up(self, light_left, light_right, blink_time=0.2):
        if light_left:
            self.left.led.blink(blink_time, blink_time)
        if light_right:
            self.right.led.blink(blink_time, blink_time)

    def lights_off(self):
        self.left.led.off()
        self.right.led.off()

    def is_restart(self):
        return self.restart.is_pressed
