import gpiozero as gz

from .gpio_interface import GPIOInterface
from .player_io import PlayerIO

LED_RIGHT_PIN = 4
LED_LEFT_PIN = 27

BUTTON_UP_RIGHT_PIN = 17
BUTTON_DOWN_RIGHT_PIN = 22

BUTTON_UP_LEFT_PIN = 23
BUTTON_DOWN_LEFT_PIN = 24

BUTTON_RESTART_PIN = 25


def get_interface():
    left_player = PlayerIO(LED_LEFT_PIN, BUTTON_UP_LEFT_PIN, BUTTON_DOWN_LEFT_PIN)
    right_player = PlayerIO(LED_RIGHT_PIN, BUTTON_UP_RIGHT_PIN, BUTTON_DOWN_RIGHT_PIN)

    restart_button = gz.Button(BUTTON_RESTART_PIN)

    return GPIOInterface(left_player, right_player, restart_button)
