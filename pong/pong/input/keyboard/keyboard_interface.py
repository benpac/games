import pygame

from ..input_interface import InputInterface, PlayerAction


class KeyboardInterface(InputInterface):
    def is_restart(self):
        keys = pygame.key.get_pressed()
        return keys[pygame.K_r] == 1

    def get_actions(self):
        actions = PlayerAction(0)
        keys = pygame.key.get_pressed()
        if keys[pygame.K_a]:
            actions |= PlayerAction.LEFT_DOWN
        if keys[pygame.K_q]:
            actions |= PlayerAction.LEFT_UP
        if keys[pygame.K_DOWN]:
            actions |= PlayerAction.RIGHT_DOWN
        if keys[pygame.K_UP]:
            actions |= PlayerAction.RIGHT_UP

        return actions

    def light_up(self, light_left, light_right, blink_time=0.2):
        pass

    def lights_off(self):
        pass


def get_interface():
    return KeyboardInterface()
