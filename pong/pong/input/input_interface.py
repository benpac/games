import abc
import enum


class PlayerAction(enum.Flag):
    LEFT_UP = enum.auto()
    LEFT_DOWN = enum.auto()
    RIGHT_UP = enum.auto()
    RIGHT_DOWN = enum.auto()


class InputInterface(abc.ABC):
    @abc.abstractmethod
    def get_actions(self):
        pass

    def get_moves(self):
        """Returns -1 for going down, 1 for going up"""
        actions = self.get_actions()
        moves = [0, 0]
        if PlayerAction.LEFT_DOWN in actions:
            moves[0] += 1
        if PlayerAction.LEFT_UP in actions:
            moves[0] -= 1
        if PlayerAction.RIGHT_DOWN in actions:
            moves[1] += 1
        if PlayerAction.RIGHT_UP in actions:
            moves[1] -= 1

        return moves

    @abc.abstractmethod
    def is_restart(self):
        pass

    @abc.abstractmethod
    def light_up(self, light_left, light_right, blink_time=0.2):
        pass

    @abc.abstractmethod
    def lights_off(self):
        pass
