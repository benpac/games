import pytest
import math

import pong.pong.physics


class TestRandomUnitVector:
    def test_is_unit(self):
        tolerance = 10**-6
        vector = pong.pong.physics.random_unit_vector()
        assert abs((vector[0]**2 + vector[1]**2) - 1) < tolerance

    @pytest.mark.parametrize("angles", [None, ((0, math.pi / 2), (math.pi, 3*math.pi/2))])
    def test_returns_2_value(self, angles):
        vector = pong.pong.physics.random_unit_vector(angles)
        assert len(vector) == 2

    @pytest.mark.parametrize("dummy", list(range(20)))
    def test_is_random(self, dummy):
        tolerance = 10**-5
        vector_1 = pong.pong.physics.random_unit_vector()
        vector_2 = pong.pong.physics.random_unit_vector()
        x_close = abs(vector_1[0] - vector_2[0]) < tolerance
        y_close = abs(vector_1[1] - vector_2[1]) < tolerance
        assert not (x_close and y_close)
